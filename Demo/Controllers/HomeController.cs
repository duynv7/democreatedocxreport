﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Xceed.Words.NET;

namespace Demo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GenerateDocument()
        {
            return View();
        }

        [HttpGet]
        public FileStreamResult File1()
        {
            string text = "abc def xyz";
            DocX docx = null;

            docx = DocX.Create(Server.MapPath("~/mydoc.docx"), DocumentTypes.Document);

            var headLineFormat = new Formatting();
            headLineFormat.FontFamily = new Font("Times New Roman");
            headLineFormat.Size = 14D;
            headLineFormat.Position = 12;
            headLineFormat.Bold = true;

            string headlineText = "Phiếu xác định nội hàm, phân tích tiêu chí tìm minh chứng tiêu chí" + Environment.NewLine + " thuộc Mức 1, 2 và 3";
            Paragraph headlineParagraph = docx.InsertParagraph(headlineText, false, headLineFormat);
            headlineParagraph.Alignment = Alignment.center;

            var paraBoldFormat = new Formatting();
            paraBoldFormat.FontFamily = new Font("Times New Roman");
            paraBoldFormat.Size = 14D;
            paraBoldFormat.Bold = true;
            paraBoldFormat.CapsStyle = CapsStyle.none;

            //Formatting Text normal 
            Formatting paraNormalFormat = new Formatting();
            paraNormalFormat.FontFamily = new Font("Times New Roman");
            paraNormalFormat.Size = 14;

            Paragraph groupParagraph = docx.InsertParagraph();
            groupParagraph.AppendLine("Nhóm công tác hoặc cá nhân: ").Bold().FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("Tiêu chuẩn: ").Bold().FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("Tiêu chí: ").Italic().FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("Mức 1:").FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("a) ").FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("b) ").FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("c) ").FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine();
            groupParagraph.Alignment = Alignment.left;
            groupParagraph.FontSize(14D);
            groupParagraph.Font("Times New Roman");
            groupParagraph.LineSpacing = 16;

            // Table
            Formatting table_header = new Formatting();
            table_header.Bold = true;
            table_header.FontFamily = new Font("Times New Roman");
            table_header.Size = 13;

            Formatting table_row = new Formatting();
            table_row.FontFamily = new Font("Times New Roman");
            table_row.Size = 13;

            Table tbl = docx.AddTable(6, 6);
            tbl.Alignment = Alignment.center;
            tbl.Design = TableDesign.TableGrid;
            tbl.AutoFit = AutoFit.Contents;

            tbl.MergeCellsInColumn(0, 0, 1);
            tbl.Rows[0].Cells[0].Paragraphs.First().InsertText("Tiêu chí", false, table_header);
            tbl.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            tbl.MergeCellsInColumn(0, 0, 1);

            tbl.MergeCellsInColumn(1, 0, 1);
            tbl.Rows[0].Cells[1].Paragraphs.First().InsertText("Nội hàm", false, table_header);
            tbl.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;

            tbl.MergeCellsInColumn(2, 0, 1);
            tbl.Rows[0].Cells[2].Paragraphs.First().InsertText("Các câu hỏi đặt ra (ứng với mỗi nội hàm)", false, table_header);
            tbl.Rows[0].Cells[2].Paragraphs.First().Alignment = Alignment.center;


            tbl.Rows[0].Cells[3].Paragraphs.First().InsertText("Minh chứng", false, table_header);
            tbl.Rows[0].Cells[3].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[1].Cells[3].Paragraphs.First().InsertText("Cần thu thập", false, table_row);
            tbl.Rows[1].Cells[3].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[1].Cells[4].Paragraphs.First().InsertText("Nơi thu thập", false, table_row);
            tbl.Rows[1].Cells[4].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[0].Cells[5].Paragraphs.First().InsertText("Ghi chú", false, table_header);
            tbl.Rows[0].Cells[5].Paragraphs.First().Alignment = Alignment.center;
            tbl.MergeCellsInColumn(5, 0, 1);
            tbl.Rows[0].MergeCells(3, 4);
            // row Mức 1, A, B, C
            tbl.Rows[2].Cells[0].Paragraphs.First().InsertText("Mức 1", false, table_row);
            tbl.Rows[3].Cells[0].Paragraphs.First().InsertText("A", false, table_row);
            tbl.Rows[4].Cells[0].Paragraphs.First().InsertText("B", false, table_row);
            tbl.Rows[5].Cells[0].Paragraphs.First().InsertText("C", false, table_row);


            for (int i = 2; i <= 5; i++)
            {
                for (int j = 1; j <= 5; j++)
                {
                    tbl.Rows[i].Cells[j].Paragraphs.First().InsertText("(" + i + "," + j + ")", false, table_row);
                }
            }
            docx.InsertTable(tbl);
            // docx.SetDirection(Direction.RightToLeft); 

            docx.Save();

            MemoryStream ms = new MemoryStream();
            docx.SaveAs(ms);
            ms.Position = 0;
            // docx.Save(ms, SaveFormat.Docx);
            var file = new FileStreamResult(ms, "application/vnd.openxmlformats-officedocx.wordprocessingml.docx")
            {
                FileDownloadName = string.Format("file1_{0}.docx", DateTime.Now.ToString("ddMMyyyyHHmmss"))
            };

            return file;
        }

        [HttpGet]
        public FileStreamResult File2()
        {
            string text = "abc_def_xyz";
            DocX docx = null;

            docx = DocX.Create(Server.MapPath("~/mydoc.docx"), DocumentTypes.Document);

            var headLineFormat = new Formatting();
            headLineFormat.FontFamily = new Font("Times New Roman");
            headLineFormat.Size = 14D;
            headLineFormat.Position = 12;
            headLineFormat.Bold = true;

            var headerFormat = new Formatting();
            headerFormat.FontFamily = new Font("Times New Roman");
            headerFormat.Size = 13D;
            headerFormat.Position = 12;
            headerFormat.Bold = true;

            var paraBoldFormat = new Formatting();
            paraBoldFormat.FontFamily = new Font("Times New Roman");
            paraBoldFormat.Size = 13D;
            paraBoldFormat.Bold = true;

            //Formatting Text normal 
            Formatting paraNormalFormat = new Formatting();
            paraNormalFormat.FontFamily = new Font("Times New Roman");
            paraNormalFormat.Size = 13;

            //Table tbls = docx.AddTable(1, 1);
            //tbls.Alignment = Alignment.left;
            //tbls.Design = TableDesign.TableGrid;
            //tbls.AutoFit = AutoFit.Window;

            //var firstRow = tbls.Rows[0].Cells[0].Paragraphs.First();
            //firstRow.AppendLine("Trường: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("Nhóm, cá nhân: ").Bold().FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("PHIẾU ĐÁNH GIÁ TIÊU CHÍ").Bold().FontSize(13D).Font("Times New Roman");
            //firstRow.Alignment = Alignment.center;
            //firstRow.AppendLine("Tiêu chuẩn: ").Bold().FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("Tiêu chí: ").Italic().FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("Mức 1: ").FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("\ta) ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("\tb) ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("\tc) ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(14D).Font("Times New Roman");
            //firstRow.AppendLine("Mức 2: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("Mức 3: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("1. Mô tả hiện trạng").FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("Mức 1: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("Mức 2: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("Mức 3: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("2. Điểm mạnh: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("3. Điểm yếu: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("4. Kế hoạch cải tiến chất lượng: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //firstRow.AppendLine("5. Tự đánh giá: ").FontSize(13D).Font("Times New Roman");
            //firstRow.Append(text).FontSize(13D).Font("Times New Roman");
            //docx.InsertTable(tbls);
            //docx.InsertParagraph();
            //string headlineText = "Phiếu đánh giá tiêu chí";
            //Paragraph headlineParagraph = docx.InsertParagraph(headlineText, false, headLineFormat);
            //headlineParagraph.Alignment = Alignment.center;

            Paragraph groupParagraphs = docx.InsertParagraph();
            groupParagraphs.AppendLine("Trường: ").FontSize(13D).Font("Times New Roman");
            groupParagraphs.Append(text).FontSize(13D).Font("Times New Roman");
            groupParagraphs.AppendLine("Nhóm, cá nhân: ").Bold().FontSize(13D).Font("Times New Roman");
            groupParagraphs.Append(text).FontSize(13D).Font("Times New Roman");

            string headerTitle = "PHIẾU ĐÁNH GIÁ TIÊU CHÍ";
            Paragraph headlineParaTitle = docx.InsertParagraph(headerTitle, false, headerFormat);
            headlineParaTitle.Alignment = Alignment.center;

            Paragraph groupParagraph = docx.InsertParagraph();
            groupParagraph.Append("Tiêu chuẩn: ").Bold().FontSize(13D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("Tiêu chí: ").Italic().FontSize(13D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("Mức 1: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("\ta) ").FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("\tb) ").FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("\tc) ").FontSize(14D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(14D).Font("Times New Roman");
            groupParagraph.AppendLine("Mức 2: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("Mức 3: ").FontSize(13D).Font("Times New Roman");

            groupParagraph.AppendLine("1. Mô tả hiện trạng").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("Mức 1: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("Mức 2: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("Mức 3: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("2. Điểm mạnh: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("3. Điểm yếu: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("4. Kế hoạch cải tiến chất lượng: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("5. Tự đánh giá: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.Alignment = Alignment.left;

            docx.Save();
            MemoryStream ms = new MemoryStream();
            docx.SaveAs(ms);
            ms.Position = 0;
            var file = new FileStreamResult(ms, "application/vnd.openxmlformats-officedocx.wordprocessingml.docx")
            {
                FileDownloadName = string.Format("file2_{0}.docx", DateTime.Now.ToString("ddMMyyyyHHmmss"))
            };

            return file;
        }

        [HttpGet]
        public FileStreamResult File3()
        {
            string text = "abc_def_xyz";
            string text2 = "\tabc_def_xyz";

            DocX docx = null;

            docx = DocX.Create(Server.MapPath("~/mydoc.docx"), DocumentTypes.Document);

            var headLineFormat = new Formatting();
            headLineFormat.FontFamily = new Font("Times New Roman");
            headLineFormat.Size = 14D;
            headLineFormat.Position = 12;
            headLineFormat.Bold = true;

            var headerFormat = new Formatting();
            headerFormat.FontFamily = new Font("Times New Roman");
            headerFormat.Size = 13D;
            headerFormat.Position = 12;
            headerFormat.Bold = true;

            Paragraph groupParagraphs = docx.InsertParagraph();
            groupParagraphs.AppendLine("Trường: ").FontSize(13D).Font("Times New Roman");
            groupParagraphs.Append(text).FontSize(13D).Font("Times New Roman");
            groupParagraphs.AppendLine("Nhóm: ").Bold().FontSize(13D).Font("Times New Roman");
            groupParagraphs.Append(text).FontSize(13D).Font("Times New Roman");

            string headerTitle = "PHIẾU ĐÁNH GIÁ TIÊU CHÍ MỨC 4";
            Paragraph headlineParaTitle = docx.InsertParagraph(headerTitle, false, headerFormat);
            headlineParaTitle.Alignment = Alignment.center;

            Paragraph groupParagraph = docx.InsertParagraph();
            groupParagraph.AppendLine("Tên tiêu chí: ").Italic().FontSize(13D).Font("Times New Roman");
            groupParagraph.Append(text).FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("1. Mô tả hiện trạng (Có mã minh chứng kèm theo) ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine(text2).FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("2. Điểm mạnh: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine(text2).FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("3. Điểm yếu: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine(text2).FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("4. Kế hoạch cải tiến chất lượng: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine(text2).FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("5. Tự đánh giá: ").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine("\tĐạt").FontSize(13D).Font("Times New Roman");
            groupParagraph.AppendLine();
            groupParagraph.Alignment = Alignment.left;

            Table t = docx.AddTable(2, 2);
            //t.SetWidths(new float[] { 190, 290 });
            t.Design = TableDesign.None;
            t.Alignment = Alignment.center;
            t.AutoFit = AutoFit.Window;
            t.Rows[0].Cells[0].Paragraphs.First().Append("Xác nhận").Bold().FontSize(13D).Font("Times New Roman");
            t.Rows[0].Cells[0].Paragraphs.First().AppendLine("của trưởng nhóm công tác").Bold().FontSize(13D).Font("Times New Roman");
            t.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[0].Cells[1].Paragraphs.First().Append("......., ngày...... tháng ....... năm....").FontSize(13D).Font("Times New Roman");
            t.Rows[0].Cells[1].Paragraphs.First().AppendLine("Người viết").Bold().FontSize(13D).Font("Times New Roman");
            t.Rows[0].Cells[1].Paragraphs.First().AppendLine("(Ký và ghi rõ họ tên)").Italic().FontSize(13D).Font("Times New Roman");
            docx.InsertTable(t);
            docx.Save();
            MemoryStream ms = new MemoryStream();
            docx.SaveAs(ms);
            ms.Position = 0;
            var file = new FileStreamResult(ms, "application/vnd.openxmlformats-officedocx.wordprocessingml.docx")
            {
                FileDownloadName = string.Format("file2_{0}.docx", DateTime.Now.ToString("ddMMyyyyHHmmss"))
            };

            return file;
        }

        [HttpGet]
        public FileStreamResult File4()
        {
            string text = "abc def xyz";
            DocX docx = null;

            docx = DocX.Create(Server.MapPath("~/mydoc.docx"), DocumentTypes.Document);

            docx.PageLayout.Orientation = Orientation.Landscape;

            var headLineFormat = new Formatting();
            headLineFormat.FontFamily = new Font("Times New Roman");
            headLineFormat.Size = 14D;
            headLineFormat.Position = 12;
            headLineFormat.Bold = true;

            string headlineText = "Bảng danh mục mã minh chứng";
            Paragraph headlineParagraph = docx.InsertParagraph(headlineText, false, headLineFormat);
            headlineParagraph.Alignment = Alignment.center;

            // Table
            Formatting table_header = new Formatting();
            table_header.Bold = true;
            table_header.FontFamily = new Font("Times New Roman");
            table_header.Size = 13;

            Formatting table_row = new Formatting();
            table_row.FontFamily = new Font("Times New Roman");
            table_row.Size = 13;

            // Hardcode số tiêu chí: có 2 tiêu chí, tiêu chí 1 có 3 mục, tiêu chí 2 có 2 mục

            var listTieuChi = Data().OrderBy(x => x.STT).ToList();
            var rowCount2 = CountData(listTieuChi);

            Table tbl = docx.AddTable(rowCount2 + 1, 7);
            tbl.Alignment = Alignment.center;
            tbl.Design = TableDesign.TableGrid;
            tbl.AutoFit = AutoFit.Contents;

            #region Header table
            tbl.Rows[0].Cells[0].Paragraphs.First().InsertText("Tiêu chí", false, table_header);
            tbl.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[0].Cells[1].Paragraphs.First().InsertText("Số TT", false, table_header);
            tbl.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[0].Cells[2].Paragraphs.First().InsertText("Mã minh chứng", false, table_header);
            tbl.Rows[0].Cells[2].Paragraphs.First().Alignment = Alignment.center;


            tbl.Rows[0].Cells[3].Paragraphs.First().InsertText("Tên minh chứng", false, table_header);
            tbl.Rows[0].Cells[3].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[0].Cells[4].Paragraphs.First().InsertText("Số, ngày ban hành, hoặc thời điểm khảo sát, điều tra, phỏng vẩn, quan sát,...", false, table_header);
            tbl.Rows[0].Cells[4].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[0].Cells[5].Paragraphs.First().InsertText("Nơi ban hành hoặc nhóm, cá nhân thực hiên", false, table_header);
            tbl.Rows[0].Cells[5].Paragraphs.First().Alignment = Alignment.center;

            tbl.Rows[0].Cells[6].Paragraphs.First().InsertText("Ghi chú", false, table_header);
            tbl.Rows[0].Cells[6].Paragraphs.First().Alignment = Alignment.center;


            //tbl.Rows[2].Cells[0].Paragraphs.First().InsertText("Mức 1", false, table_row);
            //tbl.Rows[3].Cells[0].Paragraphs.First().InsertText("A", false, table_row);
            //tbl.Rows[4].Cells[0].Paragraphs.First().InsertText("B", false, table_row);
            //tbl.Rows[5].Cells[0].Paragraphs.First().InsertText("C", false, table_row);
            #endregion


            int z = 1;
            for (int i = 0; i < listTieuChi.Count(); i++)
            {
                for (int j = 0; j < listTieuChi[i].TieuChiChilds.Count(); j++)
                {
                    tbl.Rows[z].Cells[1].Paragraphs.First().Append((j + 1).ToString()).FontSize(13D).Font("Times New Roman");
                    z++;
                }
            }

            int y = 0;
            for (int i = 0; i < listTieuChi.Count(); i++)
            {
                var childCount = listTieuChi[i].TieuChiChilds.Count();
                tbl.MergeCellsInColumn(0, y + 1, y + childCount);
                tbl.Rows[y + 1].Cells[0].Paragraphs.First().Append((i + 1).ToString()).FontSize(13D).Font("Times New Roman");
                tbl.Rows[y + 1].Cells[0].Paragraphs.First().Alignment = Alignment.center;
                y += childCount;
            }
            docx.InsertTable(tbl);
            // docx.SetDirection(Direction.RightToLeft); 

            docx.Save();

            MemoryStream ms = new MemoryStream();
            docx.SaveAs(ms);
            ms.Position = 0;
            // docx.Save(ms, SaveFormat.Docx);
            var file = new FileStreamResult(ms, "application/vnd.openxmlformats-officedocx.wordprocessingml.docx")
            {
                FileDownloadName = string.Format("file1_{0}.docx", DateTime.Now.ToString("ddMMyyyyHHmmss"))
            };

            return file;
        }

        [HttpGet]
        public FileStreamResult File5()
        {
            string text = "abc_def_xyz";
            string text2 = "\tabc_def_xyz";

            DocX docx = null;

            docx = DocX.Create(Server.MapPath("~/mydoc.docx"), DocumentTypes.Document);

            #region Header
            var headerFormat = new Formatting();
            headerFormat.FontFamily = new Font("Times New Roman");
            headerFormat.Size = 13D;
            headerFormat.Position = 12;
            headerFormat.Bold = true;

            Table t = docx.AddTable(2, 2);
            t.SetWidths(new float[] { 190, 290 });
            t.Design = TableDesign.None;
            t.Alignment = Alignment.center;
            t.AutoFit = AutoFit.ColumnWidth;

            t.Rows[0].Cells[0].Paragraphs.First().Append("TRƯỜNG" + text).FontSize(12D).Font("Times New Roman");
            t.Rows[0].Cells[0].Paragraphs.First().AppendLine("HỘI ĐỒNG TỰ ĐÁNH GIÁ").Bold().FontSize(12D).Font("Times New Roman");
            t.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[0].Cells[1].Paragraphs.First().Append("CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM").Bold().FontSize(12D).Font("Times New Roman");
            t.Rows[0].Cells[1].Paragraphs.First().AppendLine("Độc lập - Tự do -Hạnh phúc").Bold().FontSize(13D).Font("Times New Roman");
            t.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[1].Cells[0].Paragraphs.First().AppendLine("Số: " + text + "/KH " + text).FontSize(13D).Font("Times New Roman");
            t.Rows[1].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[1].Cells[1].Paragraphs.First().AppendLine("……………, ngày ... tháng ... năm ......").FontSize(14D).Font("Times New Roman");
            t.Rows[1].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            docx.InsertTable(t);

            #endregion

            #region Title
            Formatting titleFormat = new Formatting();
            titleFormat.FontFamily = new Font("Times New Roman");
            titleFormat.Size = 14;
            titleFormat.FontColor = System.Drawing.Color.Black;
            titleFormat.Bold = true;

            //Insert title  
            Paragraph paragraphTitle = docx.InsertParagraph();
            paragraphTitle.InsertText("\nKẾ HOẠCH TỰ ĐÁNH GIÁ", false, titleFormat);
            paragraphTitle.Alignment = Alignment.center;
            #endregion

            #region Body

            Paragraph body = docx.InsertParagraph();
            body.AppendLine("\tI. Mục đích tự đánh giá").Bold().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t1. Xác định cơ sở giáo dục phổ thông đáp ứng mục tiêu giáo dục trong từng giai đoạn; lập kế hoạch cải tiến chất lượng, duy trì và nâng cao chất lượng các hoạt động của nhà trường; thông báo công khai với các cơ quan quản lý nhà nước và xã hội về thực trạng chất lượng của nhà trường; để cơ quan quản lý nhà nước đánh giá, công nhận hoặc không công nhận cơ sở giáo dục phổ thông đạt kiểm định chất lượng giáo dục.").FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t2. Khuyến khích đầu tư và huy động các nguồn lực cho giáo dục, góp phần tạo điều kiện đảm bảo cho nhà trường không ngừng nâng cao chất lượng, hiệu quả giáo dục; để cơ quan quản lý nhà nước đánh giá, công nhận hoặc không công nhận cơ sở giáo dục phổ thông đạt chuẩn quốc gia.").FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t3. Các mục đích cụ thể khác (nếu có).").FontSize(14D).Font("Times New Roman");

            body.AppendLine("\n\tII. Phạm vi tự đánh giá").Bold().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t1. Đối với các trường tiểu học").Italic().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\tNhà trường triển khai hoạt động TĐG được quy định tại Thông tư số 17/2018/TT-BGDĐT.").FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t2. Đối với các trường THCS, THPT và trường phổ thông có nhiều cấp học").Italic().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\tNhà trường triển khai hoạt động TĐG được quy định tại Thông tư số 18/2018/TT-BGDĐT").FontSize(14D).Font("Times New Roman");

            body.AppendLine("\n\tIII. Công cụ tự đánh giá").Bold().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t1. Đối với trường tiểu học").Italic().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\tCông cụ TĐG là tiêu chuẩn đánh giá trường tiểu học ban hành kèm theo Thông tư số 17/2018/TT- BGDĐT và các tài liệu hướng dẫn.").FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t2. Đối với trường THCS, THPT và trường phổ thông có nhiều cấp học").Italic().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\tCông cụ TĐG là tiêu chuẩn đánh giá trường THCS, THPT và trường phổ thông có nhiều cấp học ban hành kèm theo Thông tư số 18/2018/TT-BGDĐT và các tài liệu hướng dẫn.").FontSize(14D).Font("Times New Roman");

            body.AppendLine("\n\tIV. Hội đồng tự đánh giá").Bold().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t1. Thành phần Hội đồng TĐG").Italic().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\tHội đồng TĐG được thành lập theo Quyết định số…../QĐ- ….ngày ... tháng ... năm ... của …, Hội đồng gồm có ... thành viên (Danh sách kèm theo).").FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t2. Nhóm thư ký và các nhóm công tác").Italic().FontSize(14D).Font("Times New Roman");
            body.Append("(Danh sách kèm theo).").Italic().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t3. Phân công thực hiện nhiệm vụ").Italic().FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\ta) Nhóm thư ký:").FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\t" + text).FontSize(14D).Font("Times New Roman");
            body.AppendLine("\n\tb) Các nhóm công tác, cá nhân (Có thể bao gồm: các thành viên trong Hội đồng TĐG, cán bộ, giáo viên, nhân viên,...):").FontSize(14D).Font("Times New Roman");
            body.AppendLine();
            body.Alignment = Alignment.left;


            #region Table 1
            var rowCount = 6;
            Table t1 = docx.AddTable(rowCount + 1, 5);
            t1.Alignment = Alignment.center;
            t1.Design = TableDesign.TableGrid;
            t1.Rows[0].Cells[0].Paragraphs.First().Append("TT").Bold().FontSize(13D).Font("Times New Roman");
            t1.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t1.Rows[0].Cells[1].Paragraphs.First().Append("Tiêu chí").Bold().FontSize(13D).Font("Times New Roman");
            t1.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            t1.Rows[0].Cells[2].Paragraphs.First().Append("                    ").FontSize(13D).Font("Times New Roman");
            t1.Rows[0].Cells[2].Paragraphs.First().Alignment = Alignment.center;
            t1.Rows[0].Cells[3].Paragraphs.First().Append("Nhóm công tác, cá nhân \nchịu trách nhiệm").Bold().FontSize(13D).Font("Times New Roman");
            t1.Rows[0].Cells[3].Paragraphs.First().Alignment = Alignment.center;
            t1.Rows[0].Cells[4].Paragraphs.First().Append("Ghi \nchú").Bold().FontSize(13D).Font("Times New Roman");
            t1.Rows[0].Cells[4].Paragraphs.First().Alignment = Alignment.center;

            for (int i = 1; i <= rowCount; i++)
            {
                t1.Rows[i].Cells[0].Paragraphs.First().Append(i.ToString()).FontSize(13D).Font("Times New Roman");
                t1.Rows[i].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            }

            for (int i = 1; i <= rowCount; i++)
            {
                for (int j = 1; j <= 4; j++)
                {
                    t1.Rows[i].Cells[j].Paragraphs.First().Append("(" + i + "," + j + ")").FontSize(13D).Font("Times New Roman");
                }
            }
            docx.InsertTable(t1);
            #endregion


            Paragraph body2 = docx.InsertParagraph();

            body2.AppendLine("\n\tV. Tập huấn nghiệp vụ tự đánh giá").Bold().FontSize(14D).Font("Times New Roman");
            body2.Append("(nếu có)").FontSize(14D).Font("Times New Roman");
            body2.AppendLine("\n\t1. Thời gian: ").Italic().FontSize(14D).Font("Times New Roman");
            body2.Append(text).FontSize(14D).Font("Times New Roman");
            body2.AppendLine("\n\t2. Thành phần:  ").Italic().FontSize(14D).Font("Times New Roman");
            body2.Append(text).FontSize(14D).Font("Times New Roman");
            body2.AppendLine("\n\t3. Nội dung, chương trình tập huấn,…: ").Italic().FontSize(14D).Font("Times New Roman");
            body2.Append(text).FontSize(14D).Font("Times New Roman");
            body2.AppendLine();
            body2.AppendLine("\n\tVI. Dự kiến các nguồn lực (nhân lực, tài chính,...) và thời điểm cần huy động/cung cấp").Bold().FontSize(14D).Font("Times New Roman");
            body2.AppendLine("\n\t1. Đối với các tiêu chí Mức 1, 2 và 3 ").Italic().FontSize(14D).Font("Times New Roman");

            #region Table2

            var listTieuChi = Data().OrderBy(x => x.STT).ToList();

            var rowCount2 = CountData(listTieuChi);

            Table t12 = docx.AddTable(rowCount2 + 1, 5);
            t12.Alignment = Alignment.center;
            t12.Design = TableDesign.TableGrid;
            t12.Rows[0].Cells[0].Paragraphs.First().Append("Tiêu chuẩn").Bold().FontSize(13D).Font("Times New Roman");
            t12.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t12.Rows[0].Cells[1].Paragraphs.First().Append("Tiêu chí").Bold().FontSize(13D).Font("Times New Roman");
            t12.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            t12.Rows[0].Cells[2].Paragraphs.First().Append("Các nguồn lực cần huy động/cung cấp").Bold().FontSize(13D).Font("Times New Roman");
            t12.Rows[0].Cells[2].Paragraphs.First().Alignment = Alignment.center;
            t12.Rows[0].Cells[3].Paragraphs.First().Append("Nhóm công tác, cá nhân \nchịu trách nhiệm").Bold().FontSize(13D).Font("Times New Roman");
            t12.Rows[0].Cells[3].Paragraphs.First().Alignment = Alignment.center;
            t12.Rows[0].Cells[4].Paragraphs.First().Append("Ghi \nchú").Bold().FontSize(13D).Font("Times New Roman");
            t12.Rows[0].Cells[4].Paragraphs.First().Alignment = Alignment.center;


            int z = 1;
            for (int i = 0; i < listTieuChi.Count(); i++)
            {
                for (int j = 0; j < listTieuChi[i].TieuChiChilds.Count(); j++)
                {
                    t12.Rows[z].Cells[1].Paragraphs.First().Append("Tiêu chí " + (i + 1) + "." + (j + 1)).FontSize(13D).Font("Times New Roman");
                    z++;
                }
            }

            int y = 0;
            for (int i = 0; i < listTieuChi.Count(); i++)
            {
                var childCount = listTieuChi[i].TieuChiChilds.Count();
                t12.MergeCellsInColumn(0, y + 1, y + childCount);
                t12.Rows[y + 1].Cells[0].Paragraphs.First().Append((i + 1).ToString()).FontSize(13D).Font("Times New Roman");
                t12.Rows[y + 1].Cells[0].Paragraphs.First().Alignment = Alignment.center;
                y += childCount;
            }

            docx.InsertTable(t12);
            #endregion

            Paragraph body3 = docx.InsertParagraph();

            body3.AppendLine("\n\t2. Đối với các tiêu chí Mức 4").Italic().FontSize(14D).Font("Times New Roman");
            body3.Append("(nếu có)").FontSize(14D).Font("Times New Roman");


            #region Table3

            Table t123 = docx.AddTable(listTieuChi.Count() + 1, 4);
            t123.Alignment = Alignment.center;
            t123.Design = TableDesign.TableGrid;
            t123.Rows[0].Cells[0].Paragraphs.First().Append("Tiêu chí").Bold().FontSize(13D).Font("Times New Roman");
            t123.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t123.Rows[0].Cells[1].Paragraphs.First().Append("Các nguồn lực cần huy động/cung cấp").Bold().FontSize(13D).Font("Times New Roman");
            t123.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            t123.Rows[0].Cells[2].Paragraphs.First().Append("Thời điểm cần huy động").Bold().FontSize(13D).Font("Times New Roman");
            t123.Rows[0].Cells[2].Paragraphs.First().Alignment = Alignment.center;
            t123.Rows[0].Cells[3].Paragraphs.First().Append("Ghi \nchú").Bold().FontSize(13D).Font("Times New Roman");
            t123.Rows[0].Cells[3].Paragraphs.First().Alignment = Alignment.center;


            for (int i = 0; i < listTieuChi.Count(); i++)
            {

                t123.Rows[i + 1].Cells[0].Paragraphs.First().Append("Tiêu chí " + (i + 1)).FontSize(13D).Font("Times New Roman");
            }

            docx.InsertTable(t123);
            #endregion


            Paragraph body4 = docx.InsertParagraph();

            body4.AppendLine("\n\tVII. Dự kiến thuê chuyên gia tư vấn để giúp Hội đồng triển khai TĐG").Bold().FontSize(14D).Font("Times New Roman");
            body4.Append("(nếu có)").FontSize(14D).Font("Times New Roman");
            body4.AppendLine("\n\tXác định các lĩnh vực cần thuê chuyên gia, mục đích thuê chuyên gia, vai trò của chuyên gia, số lượng chuyên gia, thời gian cần thuê chuyên gia, kinh phí thuê chuyên gia,...").FontSize(14D).Font("Times New Roman");

            body4.AppendLine("\n\tVIII. Lập Bảng danh mục mã minh chứng").Bold().FontSize(14D).Font("Times New Roman");
            body4.AppendLine("\n\tSau khi các nhóm công tác, cá nhân thực hiện xác định nội hàm, phân tích tiêu chí tìm minh chứng cho từng tiêu chí; phân loại và mã hoá các minh chứng thu được. Hội đồng TĐG thảo luận các minh chứng cho từng tiêu chí đã thu thập được và lập Bảng danh mục mã minh chứng.").FontSize(14D).Font("Times New Roman");
            body4.AppendLine("\tBảng danh mục mã minh chứng được trình bày bảng theo chiều ngang của khổ A4 (có thể để riêng và sau đó để ở phần Phụ lục của báo cáo TĐG).").FontSize(14D).Font("Times New Roman");
            body4.AppendLine("\n\tIX. Thời gian và nội dung hoạt động").Bold().FontSize(14D).Font("Times New Roman");
            body4.AppendLine("\n\tTuỳ theo từng điều kiện cụ thể, mỗi trường xác định thời gian thực hiện phù hợp để hoàn thành hoạt động TĐG. Sau đây là ví dụ minh họa về thời gian và nội dung hoạt động triển khai thực hiện hoạt động TĐG:").FontSize(14D).Font("Times New Roman");


            #region Table4

            Table t1234 = docx.AddTable(9, 2);
            t123.Alignment = Alignment.center;
            t1234.Design = TableDesign.TableGrid;
            t1234.AutoFit = AutoFit.Window;
            t1234.Rows[0].Cells[0].Paragraphs.First().Append("Thời gian").Bold().FontSize(13D).Font("Times New Roman");
            t1234.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t1234.Rows[0].Cells[1].Paragraphs.First().Append("Nội dung hoạt động").Bold().FontSize(13D).Font("Times New Roman");
            t1234.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;

            t1234.Rows[1].Cells[0].Paragraphs.First().Append("Tuần 1").Bold().FontSize(13D).Font("Times New Roman");
            t1234.Rows[1].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t1234.Rows[1].Cells[1].Paragraphs.First().Append("1. Họp lãnh đạo nhà trường để thảo luận dự kiến các thành viên Hội đồng TĐG và các vấn đề liên quan đến triển khai hoạt động TĐG.\n2. Hiệu trưởng ra quyết định thành lập Hội đồng TĐG.\n3. Họp Hội đồng TĐG để: \n- Công bố quyết định thành lập Hội đồng TĐG;\n- Thảo luận về nhiệm vụ cụ thể cho từng thành viên Hội đồng TĐG; phân công nhiệm vụ cụ thể cho các nhóm công tác và cá nhân;\n- Dự kiến thuê chuyên gia tư vấn để giúp Hội đồng TĐG triển khai hoạt động TĐG (nếu có);\n- Dự thảo và ban hành Kế hoạch TĐG.\n4. Phổ biến Kế hoạch TĐG đến toàn thể cán bộ, giáo viên, nhân viên của nhà trường và các bên liên quan.").FontSize(13D).Font("Times New Roman");
            t1234.Rows[1].Cells[1].Paragraphs.First().Alignment = Alignment.left;

            t1234.Rows[2].Cells[0].Paragraphs.First().Append("Tuần 2").Bold().FontSize(13D).Font("Times New Roman");
            t1234.Rows[2].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t1234.Rows[2].Cells[1].Paragraphs.First().Append("1. Tổ chức hội thảo/tập huấn/hội nghị về nghiệp vụ TĐG cho toàn thể cán bộ, giáo viên, nhân viên của trường và các bên liên quan.\n2. Các nhóm công tác, cá nhân thực hiện xác định nội hàm, phân tích tiêu chí tìm minh chứng cho từng tiêu chí.").FontSize(13D).Font("Times New Roman");
            t1234.Rows[2].Cells[1].Paragraphs.First().Alignment = Alignment.left;

            t1234.Rows[3].Cells[0].Paragraphs.First().Append("Tuần 3 - 5").Bold().FontSize(13D).Font("Times New Roman");
            t1234.Rows[3].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t1234.Rows[3].Cells[1].Paragraphs.First().Append("1. Các nhóm công tác, cá nhân thực hiện:\n- Xác định nội hàm, phân tích tiêu chí tìm minh chứng cho từng tiêu chí (tiếp theo việc tuần 2);\n- Phân loại và mã hoá các minh chứng thu được.\n2. Hội đồng TĐG thảo luận các minh chứng cho từng tiêu chí đã thu thập được và lập Bảng danh mục mã minh chứng.\n3. Các nhóm chuyên trách, cá nhân viết các Phiếu đánh giá tiêu chí. ").FontSize(13D).Font("Times New Roman");
            t1234.Rows[3].Cells[1].Paragraphs.First().Alignment = Alignment.left;

            docx.InsertTable(t1234);
            #endregion

            #endregion




            docx.Save();
            MemoryStream ms = new MemoryStream();
            docx.SaveAs(ms);
            ms.Position = 0;
            var file = new FileStreamResult(ms, "application/vnd.openxmlformats-officedocx.wordprocessingml.docx")
            {
                FileDownloadName = string.Format("file2_{0}.docx", DateTime.Now.ToString("ddMMyyyyHHmmss"))
            };

            return file;
        }

        public List<TieuChi> Data()
        {
            var tieuChiChild1 = new List<TieuChiChild>();
            tieuChiChild1.Add(new TieuChiChild() { Id = 1, Ten = "TC 1.1", STT = 1 });
            tieuChiChild1.Add(new TieuChiChild() { Id = 2, Ten = "TC 1.2", STT = 2 });
            tieuChiChild1.Add(new TieuChiChild() { Id = 3, Ten = "TC 1.3", STT = 3 });
            var tieuChi1 = new TieuChi()
            {
                Id = 1,
                Ten = "TC 1",
                STT = 1,
                TieuChiChilds = tieuChiChild1.OrderBy(x => x.STT).ToList()
            };

            var tieuChiChild2 = new List<TieuChiChild>();
            tieuChiChild2.Add(new TieuChiChild() { Id = 4, Ten = "TC 2.1", STT = 1 });
            tieuChiChild2.Add(new TieuChiChild() { Id = 5, Ten = "TC 2.2", STT = 2 });
            var tieuChi2 = new TieuChi()
            {
                Id = 2,
                Ten = "TC 2",
                STT = 2,
                TieuChiChilds = tieuChiChild2.OrderBy(x => x.STT).ToList()
            };

            var tieuChis = new List<TieuChi>();
            tieuChis.Add(tieuChi1);
            tieuChis.Add(tieuChi2);
            return tieuChis;
        }

        public int CountData(List<TieuChi> data)
        {
            int count = 0;

            for (int i = 0; i < data.Count(); i++)
            {
                count += data[i].TieuChiChilds.Count();
            }
            return count;
        }
    }

    public class TieuChi
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public int STT { get; set; }
        public List<TieuChiChild> TieuChiChilds { get; set; }

    }
    public class TieuChiChild
    {
        public int Id { get; set; }
        public string Ten { get; set; }
        public int STT { get; set; }
    }
}